#!/bin/bash

source /etc/os-release

function ssh_prepare() {
sshkey_path="$HOME/.ssh"
if [ ! -e "$sshkey_path" ];then
  mkdir "$sshkey_path"
fi
if [ ! -e "$sshkey_path/id_rsa" ];then
  ssh-keygen -f "$sshkey_path"/id_rsa -t rsa -N '' -q
else
  echo y | ssh-keygen -f "$sshkey_path"/id_rsa -t rsa -N '' -q
fi
cat "$sshkey_path"/id_rsa.pub >> "$sshkey_path"/authorized_keys
}

function detect_setup_testenv() {
if [[ $NAME == "CentOS Stream" ]] &&  [[ -e /run/ostree-booted ]]; then
    wget -O /etc/yum.repos.d/beaker-harness.repo https://beaker-project.org/yum/beaker-harness-CentOSStream.repo
    sed -i 's/\$stream/9-stream/g' /etc/yum.repos.d/centos.repo
    sed -i 's/\$stream/9-stream/g' /etc/yum.repos.d/centos-addons.repo
    cat > /etc/environment << EOF
RSTRNT_PKG_CMD=rpm-ostree
RSTRNT_PKG_ARGS="-A --idempotent --allow-inactive"
cat > /etc/environment << EOF
EOF
fi
}

detect_setup_testenv

# temp workaround while no rpm-ostree support in tmt 1095
if rpm-ostree -A --idempotent --allow-inactive install restraint restraint-client openssh || \
dnf -y install restraint restraint-client openssh; then
  echo "install restraint and client success"
else
  echo "Restraint install issue"
  exit $?
fi

ssh_prepare
