# Restraint Helper Plan and Test example
 
Included are a TMT preparation plan, with helper script and a TMT test.fmf with an example restraint xml
 
## Getting started
 
TMT run example: `tmt run -a test -n test`

-vvv enables each level of verbosity 
 
## How does it work
 
The preparation script performs some basic setup tasks required for RHIVOS testing. Required repos are added and configured. Restraint, restraint-client, and openssh are installed.
 
TMT will call test.fmf by the -n test parameter supplied. The test is associated to the plan via tag:restraint and called in TMT's prepare step. After the prepare is performed, the SUT will be configured and ready for the test, which is begun with the TMT test line entry in the test.fmf file.
 
Key note: this configuration is setup to run restraint in standalone mode, on the localhost ie SUT. This also is currently configured to disable avc_errors which are selinux related issues - this needs to be recognized and adjusted if needed

Addtionally, in test.fmf the "test: restraint..." is the test entry point, this is configured to use the job - restraint-tests.xml against the host - localhost. Restraint can be configured on a host machine to remote into a SUT, the requirments on the SUT are openssh and restraint for this to be sucessful. 

One could use TMT to connect to a ostree deployment for initial configuration, and then use a reomote host to run addtional tests via restraint.
